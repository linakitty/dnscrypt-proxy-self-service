[TOC]

# Configure dnscrypt-proxy and dnsmasq on Linux

## Important

This introduction is test under Fedora 28 and 29. Other Linux distributions maybe work as well.

## How to configure

### Download dnscrypt-proxy

First go to [dnscrypt-proxy](https://github.com/jedisct1/dnscrypt-proxy) official page to download the latest dnscrypt-proxy version. Keep  the executable file `dnscrypt-proxy` and `public-resolvers.md`, `public-resolvers.md.minisig`.

### Download configure files

Go to [dnscrypt-proxy-config](https://github.com/CNMan/dnscrypt-proxy-config), download the configure filess, especially `dnscrypt-proxy.toml`. You can download all and save to the same folder as executable file `dnscrypt-proxy` in your `home`, for example `dnscrypt`. I've change the listening port to `5300`.

```
listen_addresses = ['127.0.0.1:5300', '[::1]:5300']
```

The other files like `dnscrypt-whitelist.txt`, `dnscrypt-blacklist-ips.txt`, `dnscrypt-cloaking-rules.txt` are from [dnsmasq-china-list](https://github.com/felixonmars/dnsmasq-china-list), use as your wish.

### Test dnscrypt-proxy

Go to the dnscrypt-proxy directory `dnscrypt`, and type:

```
$ ./dnscrypt-proxy
```

If nothing happens, that's great, go to the next step. If see any errors, please check https://github.com/jedisct1/dnscrypt-proxy/wiki/Installation-linux.

### Configure Network-Manager and resolv

Do not let Network Manager control dns,use your local dns server. Edit `/etc/NetworkManager/NetworkManager.conf` and add the following to the `[main]` section:

```
$ sudo vi /etc/NetworkManager/NetworkManager.conf

[main]
...
dns=none
```

Now edit `/etc/resolv.conf` and change to this:

```
nameserver 127.0.0.1
options edns0 single-request-reopen
```

### Configure dnsmasq and Network-Manager

Install dnsmasq:

```
$ sudo dnf install dnsmasq
$ sudo systemctl enable dnsmasq
```

Edit `dnsmasq.conf`, you can just configure like ours. The most important is server:

```
server=::1#5300
server=127.0.0.1#5300
```

Test dnsmasq syntax:

```
$ dnsmasq --test 
```

If all are OK, we are almost finished.

Start dnsmasq service:

```
$ sudo systemctl start dnsmasq
$ sudo systemctl restart NetworkManager
```

## Verify

```
$ sudo netstat -antup|grep 53
```

## Test DNS

Go to this page to check your DNS: [https://dnsleaktest.com/](https://dnsleaktest.com/)


## Test dnsmasq

Maybe you need `bind-utils.x86_64`.

First time

```
$ dig archlinux.org | grep "Query time"

;; Query time: 445 msec
```
Second time

```
$ dig archlinux.org | grep "Query time"

;; Query time: 0 msec
```

## FAQ

### Dnscrypt-proxy start on boot

The github page of dnscrypt-proxy provides an introduction to configure auto boot service. But `dnscrypt-proxy -service install` doesn't work after reboot, especially the RedHat like systems.

You can use desktop enviroment like xfce `Session and Startup` or just `cd` to your dnscrypt folder `dnscrypt`, and type `./dnscrypt-proxy` each time.

Or creat an autostart entry:

```
$ vi ~/.config/autostart/dnscrypt.desktop

[Desktop Entry]
Encoding=UTF-8
Version=0.9.4
Type=Application
Name=Dnscrypt
Comment=Encrypt dns transfer
Exec=/home/yourname/dnscrypt/dnscrypt-proxy
OnlyShowIn=XFCE;
StartupNotify=false
Terminal=false
Hidden=false
```
